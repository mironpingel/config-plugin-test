<?php

namespace ConfigPluginTest\Helper;

use Plenty\Plugin\Log\Loggable;
use Plenty\Plugin\ConfigRepository;

class Logger
{
    use Loggable;

    public function __construct()
    {
        //
    }

    public function error($identifikator, $message, $value)
    {
        $this->getLogger($identifikator)->error($message, $value);
    }
}
