<?php
namespace ConfigPluginTest\Providers;

use Plenty\Plugin\RouteServiceProvider;
use Plenty\Plugin\Routing\Router;

class PluginRouteServiceProvider extends RouteServiceProvider
{
    /**
	 * @param Router $router
	 */
	public function map(Router $router)
	{
		$router->get('mironconfigtest', 'ConfigPluginTest\Controllers\ContentController@showconfig');
	}

}
