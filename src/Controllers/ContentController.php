<?php

namespace ConfigPluginTest\Controllers;

use ConfigPluginTest\Helper\Logger;
use Plenty\Plugin\ConfigRepository;
use Plenty\Plugin\Controller;
use Plenty\Plugin\Templates\Twig;

class ContentController extends Controller
{

    private $log;

    /**
     * ContentController constructor.
     * @param Log $log
     */
    public function __construct(Logger $log)
    {
        $this->log = $log;
    }


    /**
	 * @param Twig $twig
	 * @return string
	 */
	public function showconfig(Twig $twig, ConfigRepository $config):string
	{
        $userInput = $config->get("ConfigPluginTest.global.testinput");
        $testVariable = "IchBinEineBiene";

        $this->log->error('TESTINPUT', 'Der inhalt', $testVariable);
        $this->log->error('CONFIGINPUT', 'Der inhalt', $userInput);

        return $twig->render('ConfigPluginTest::hello', [
            "someinput" => $userInput
        ]);
	}
}
